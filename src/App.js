import React, { Component } from 'react';
import './App.css';
import SearchPatient from "./containers/SearchPatient";
import AddPatient from "./containers/AddPatient";
import PatientDetail from "./components/patient-detail/PatientDetail";
import { Route, Switch, Link } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='patient/12345'>Patient Page</Link></li>
        </ul>
        
        <div>
          <Switch>
            <Route exact path="/" component={SearchPatient}/>
            <Route path="/patient/:id" component={PatientDetail}/>          
            <Route path="/add" component={AddPatient}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
