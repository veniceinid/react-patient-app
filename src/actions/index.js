export const addPatient = (patient) => ({
    type: 'ADD_PATIENT',
    patient
  })
  
  export const setSearchFilter = (filter) => ({
    type: 'SET_SEARCH_FILTER',
    filter
  })
  
  export const removePatient = (patientIds) => ({
    type: 'REMOVE_PATIENT',
    patientIds
  })

  