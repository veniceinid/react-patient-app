import React, { Component } from 'react';
import './PatientDetail.css';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Card } from 'antd';

class PatientDetail extends Component {

  patient = {};
        
  constructor(props) {
    super(props);
    console.log("PatientDetail: " + props.match.params.id);
    this.patient = this.lookupPatient(props.match.params.id, this.props.patients);

    if (this.patient) {
      console.log("Found patient " + props.match.params.id);
    } else {
      this.patient = {};
      console.log("I could not find patient with id: " + props.match.params.id);
    }
  }

  lookupPatient(patientId, patients) {
    return patients.find(patient => 
      (patient.id === Number(patientId)) ? patient : null
    )
  }

  render() {
    return (
      <div style={{ background: '#ECECEC', padding: '30px' }}>
          <Card title={this.patient.firstName + ' ' + this.patient.lastName + ' (' + this.patient.id + ')'} 
                extra={<a href="">More</a>} style={{ width: 400 }} bordered>
            <div><strong>Insurance name</strong> {this.patient.insuranceName}</div>
            <div><strong>Insurance Id</strong> {this.patient.insuranceId}</div>
            <div><strong>Email</strong> {this.patient.email}</div>
            <div><strong>Phone</strong> {this.patient.phone}</div>
            <div><strong>Registration Notes</strong> {this.patient.regComment}</div>
          </Card>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      patients: state.patients
  }
}

export default withRouter(connect(mapStateToProps, null)(PatientDetail));

