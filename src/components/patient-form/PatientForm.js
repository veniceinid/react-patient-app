import React, { Component } from 'react';
import './PatientForm.css';
import { Button, Form, Input, message } from 'antd';

const FormItem = Form.Item;

class PatientForm extends Component {
  constructor(props) {
    super(props);
    console.log("Add patient form, total number of patients: " + this.props.patients.length);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="patient">
          <Form layout="horizontal">
            <FormItem>
              {getFieldDecorator('firstName', {
                rules: [{ required: true, message: "Enter patient's first name."}]
              })( <Input placeholder="First Name"/> )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('lastName', {
                rules: [{ required: true, message: "Enter patient's last name."}]
              })( <Input placeholder="Last Name"/> )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('insuranceName', {
                // rules: [{ required: true, message: "Enter patient's insurance."}]
              })( <Input placeholder="Insurance name"/> )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('insuranceId', {
                // rules: [{ required: true, message: "Enter patient's first name."}]
              })( <Input placeholder="Insurance Id"/> )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('email', {
                rules: [{ type: 'email', message: "Enter a valid email address."}]
              })( <Input placeholder="Email"/> )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('phone', {
              })( <Input placeholder="Phone #"/> )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('regComment', {
              })( <Input autosize={{ minRows: 4, maxRows: 6 }} placeholder="Registration Comment"/> )}
            </FormItem>

            <FormItem><Button type="primary" onClick={this.addPatient}>Add Patient</Button></FormItem>
          </Form>
      </div>
    );
  }

  //automatically bind this to the function
  addPatient = () => {
    this.props.form.validateFields((err, patient) => {
      if (!err) {
        console.log('Received values of form: ', patient);
        this.props.onAddPatient(patient);
        message.success("Patient has been successfully added.");
        this.props.history.push('/');
      } else {
        console.error("An error has occurred: " + err);
      }
    });
  }
}

export default Form.create({})(PatientForm);

