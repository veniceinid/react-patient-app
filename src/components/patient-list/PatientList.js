import React, { Component } from 'react';
import './PatientList.css';
import { Table } from 'antd';
import { Button, Popconfirm, message } from 'antd';
import { Link } from 'react-router-dom';

const columns = [{
  title: 'Patient Id',
  dataIndex: 'id',
  key: 'id',
}, {
  title: 'First Name',
  dataIndex: 'firstName',
  key: 'firstName',
}, {
  title: 'Last Name',
  dataIndex: 'lastName',
  key: 'lastName',
}, {
  title: 'Insurance',
  dataIndex: 'insuranceName',
  key: 'insuranceName',
},
{
  title: 'Insurance Id',
  dataIndex: 'insuranceId',
  key: 'insuranceId',
},
{
  title: 'Email',
  dataIndex: 'email',
  key: 'email',
},
{
  title: 'Phone',
  dataIndex: 'phone',
  key: 'phone',
},
{
  title: 'Comment',
  dataIndex: 'regComment',
  key: 'regComment',
}];

export default class PatientList extends Component {
  patients = [];
  selectedPatientIds = [];
  
  constructor(props) {
    super(props);
    this.patients = this.props.patients;
    this.onAddPatient = this.props.onAddPatient;
    this.state = {
      hasSelectedPatient: false,
    };
  }
  
  confirmRemovePatients = (e) => {
    console.log(this.selectedPatientIds);
    if (this.selectedPatientIds.length > 0) {
      message.success('Selected patients will be removed: ' + this.selectedPatientIds);    
    } 
    this.props.onPatientRemove(this.selectedPatientIds);
    console.log("Patients after removing selected patients: " + this.patients.length); 
    console.log("Patients after removing selected patients: " + this.props.patients.length); 
    console.log(this.props.patients);
  }
  
  cancelRemovePatients = (e) => {
    message.error('No patients were removed.');
  }

  render() {
    console.log("Total number of patients: " + this.patients.length);  
    return (
      <div className="search">
        <div>
          <Link to='/add'><Button className="action" type="primary">Add Patient</Button></Link>
          <Popconfirm title="Are you sure to remove the selected patients?" onConfirm={this.confirmRemovePatients} onCancel={this.cancelRemovePatients} okText="Yes" cancelText="No">
            <Button disabled={!this.state.hasSelectedPatient} className="action" type="primary">Remove Patient(s)</Button>
          </Popconfirm>
        </div>

        <Table 
          dataSource={this.patients} 
          columns={columns} 
          rowKey="id"
          pagination={{pageSize:5}}
          onRowClick={this.onRowClick}  
          rowSelection={this.rowSelection}      
          />
      </div>
    );
  }

  onRowClick = (record, index, event) => {
    console.log("Navigating to patient info page of " + record.firstName + " " + record.lastName +  " (" + record.id + ")");
    this.props.history.push('/patient/' + record.id);
  }

  addPatient() {
    console.log("Navigating to add patient screen...");
  }

  rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      this.selectedPatientIds = selectedRowKeys;
      if (this.selectedPatientIds.length > 0) {
        this.setState({hasSelectedPatient: true});
      } else {
        this.setState({hasSelectedPatient: false});       
      }
    },
  }
  
}
