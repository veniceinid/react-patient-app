import { connect } from 'react-redux';
import PatientForm from '../components/patient-form/PatientForm';
import { addPatient } from '../actions';

const mapStateToProps = state => {
    return {
        patients: state.patients
    }
}

const mapDispatchToProps = {
    onAddPatient: addPatient
}

const AddPatient = connect (
    mapStateToProps,
    mapDispatchToProps
)(PatientForm)

export default AddPatient
