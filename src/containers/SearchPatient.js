import { connect } from 'react-redux';
import PatientList from '../components/patient-list/PatientList';
import { removePatient } from '../actions';


const getFilteredPatients = (patients, filter) => {
    switch (filter) {
        case 'ALL': return patients;
        case 'TEST': return patients;
        default:
            throw new Error('Unknown filter: ' + filter);
    }
}

const mapStateToProps = state => {
    return {
        patients: getFilteredPatients(state.patients, state.searchFilter)
    }
}

const mapDispatchToProps = {
    onPatientRemove: removePatient
}

const SearchPatient = connect (
    mapStateToProps,
    mapDispatchToProps
)(PatientList)

export default SearchPatient