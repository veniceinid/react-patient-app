import 'antd/dist/antd.css';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import patientApp from './reducers';

let store = createStore(patientApp)

ReactDOM.render(
    (
        <Provider store={store}>
            <BrowserRouter >
                <Route path="/" component={App} />
            </BrowserRouter>
        </Provider>
    ), document.getElementById('root'));
registerServiceWorker();
