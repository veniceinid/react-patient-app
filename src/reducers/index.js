import { combineReducers } from 'redux';
import patients from './patients';
import searchFilter from './searchFilter';

const patientApp = combineReducers({
  patients,
  searchFilter
})

export default patientApp
